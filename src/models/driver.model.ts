export class DriverModel {
    constructor(
        public id?: number,
        public nome?: string,
        public img?: string,
        public latitude?: number,
        public longitude?: number,
        public kmTotal?: number
    ) {
    }
}
