import React, {useEffect} from 'react';
import {Text, View} from 'react-native';
import {Container, ButtonContainer, ReturnButton} from './styles';
import {Icon} from 'react-native-elements';
import {DriverDetails} from '../../components/DriverDetails';

export default function Dashboard({navigation}: any) {

    useEffect(() => {

    }, []);

    return (
        <Container>
            <ButtonContainer>
                <ReturnButton onPress={() => navigation.goBack()}>
                    <Icon
                        name='arrow-left'
                        type='material-community'
                        color='#898383'
                    />
                </ReturnButton>
            </ButtonContainer>
            <View>
                <DriverDetails></DriverDetails>
            </View>
        </Container>
    )
};

