import styled from 'styled-components/native'

export const Container = styled.View`
    flex: 1;
    backgroundColor: #F5FCFF;
    width: 100%;
`;

export const ButtonContainer = styled.View`
    justify-content: space-between;
    background: transparent;
    flex-direction: row;
    height: 80px;
    padding: 20px;
    width: 100%;
`;

export const ReturnButton = styled.TouchableOpacity`
    justify-content: center;
    background: transparent;
    elevation: 4;
    
    width: 20px;
`;

export const DashboardButton = styled.TouchableOpacity`
    justify-content: center;
    background: white;
    border-radius: 50px;
    elevation: 4;
    width: 55px;
    height: 55px;
`;
