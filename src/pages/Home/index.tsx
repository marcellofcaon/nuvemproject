import React, {useEffect, useState} from 'react';
import {Animated, StyleSheet} from 'react-native';
import {Icon} from 'react-native-elements';
import {ButtonContainer, Container, DashboardButton, RefreshButton} from './styles';
import MapView, {Marker} from 'react-native-maps';
import {
    useJourneyDrivers, useJourneyHidden,
    useJourneySetDriversRequest, useJourneySetHidden,
    useJourneySetKmValueRequest,
    useJourneySetSelectedDriverRequest
} from '../../store/hooks/journey';
import {TripDetails} from '../../components/TripDetails';


export default function Home({navigation}: any) {
    const setKmValue = useJourneySetKmValueRequest();
    const setDrivers = useJourneySetDriversRequest();
    const drivers = useJourneyDrivers();
    const setSelectedDriver = useJourneySetSelectedDriverRequest();
    const isHidden = useJourneyHidden();
    const setJourneyHidden = useJourneySetHidden();
    const [bounceValue, setBounceValue] = useState(new Animated.Value(500));
    const toggleSubview = (map: boolean, driver?: any) => {
        setSelectedDriver(driver);
        let toValue = 500;
        if(isHidden && !map) {
            toValue = 0;
        }
        Animated.spring(
            bounceValue,
            {
                toValue: toValue,
                velocity: 3,
                tension: 2,
                friction: 8,
                useNativeDriver: true
            }
        ).start();

        setJourneyHidden(!isHidden);
    }

    const startJourney = () => {
        setKmValue((Math.random() * (10 - 1) + 1).toFixed(2));
        startMock();
    }

    const startMock = () => {
        setDrivers();
    }

    useEffect(() => {
        startJourney();
    }, []);

    return (
        <Container>
            <ButtonContainer>
                <RefreshButton onPress={() => startJourney()}>
                    <Icon
                        name='refresh'
                        type='material'
                        color='#898383'
                    />
                </RefreshButton>
                <DashboardButton onPress={() => navigation.navigate('Dashboard')}>
                    <Icon
                        name='bell-outline'
                        type='material-community'
                        color='#898383'
                    />
                </DashboardButton>
            </ButtonContainer>

            <MapView
                onPress={() => toggleSubview(true, null)}
                style={styles.map}
                initialRegion={{
                    latitude: -15.795929,
                    longitude: -47.880336,
                    latitudeDelta: 0.0124,
                    longitudeDelta: 0.0135,
                }}
            >
                {drivers ? drivers.map((marker: any, index: number) => (
                    <Marker
                        onPress={() => toggleSubview(false, marker)}
                        key={index}
                        coordinate={{ latitude : marker.latitude , longitude : marker.longitude }}
                    >
                        <Icon
                            name='directions-car'
                            type='material'
                            color='#FF0000FF'
                        />
                    </Marker>
                )):<></>}
            </MapView>
            <Animated.View
                style={[styles.subView,
                    {transform: [{translateY: bounceValue}]}]}>
                <TripDetails></TripDetails>
            </Animated.View>
        </Container>
    )
};

const styles = StyleSheet.create({
    subView: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#FFFFFF',
        height: 500,
        borderTopRightRadius: 32,
        borderTopLeftRadius: 32,
        padding: 20
    },
    map: {
        position: 'absolute',
        flex: 1,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: -1
    },
});
