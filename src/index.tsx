import 'react-native-gesture-handler';
import * as React from 'react';
import {Provider} from 'react-redux';
import configureStore from '../src/store';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';

const {store} = configureStore();

const Stack = createStackNavigator();

const App = () => {
    return (
        <Provider store={store}>
            <SafeAreaProvider>
                <NavigationContainer>
                    <Stack.Navigator initialRouteName="Home" screenOptions={{headerShown: false}}>
                        <Stack.Screen name="Home" component={Home} />
                        <Stack.Screen name="Dashboard" component={Dashboard} />
                    </Stack.Navigator>
                </NavigationContainer>
            </SafeAreaProvider>
        </Provider>
    );
};


export default App;
