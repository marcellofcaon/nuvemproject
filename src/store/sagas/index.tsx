import {all} from 'redux-saga/effects';
import {journeySagas} from './journey';

export default function* rootSaga() {
  yield all([
      ...journeySagas
  ]);
}
