import {call, put, select, takeLatest} from 'redux-saga/effects';
import {Creators, Types} from '../reducers/journey';
const customData = require('../../assets/mock/mock.json');

export function* setKmValue(kmValue) {
  try {
    console.log('kmValue setted')
  } catch (error) {
    console.log('Não foi possível atualizar valor da kilometragem')
  }
}

export function* setKmLimit(kmLimit) {
  try {
    console.log('kmLimit setted')
  } catch (error) {
    console.log('Não foi possível atualizar kilometragem limite')
  }
}
export function* updateDriverJourney(value) {
  try {
    const {selectedDriver, drivers, kmValue} = yield select(state => state.journey);

    let driverIndex = drivers.findIndex(driver => driver.id === selectedDriver.id);

    drivers[driverIndex].kmLimit = drivers[driverIndex].kmLimit - +value.distance;
    drivers[driverIndex].dailyRevenue = drivers[driverIndex].dailyRevenue + (kmValue * +value.distance);

    yield put(Creators.setDriversSuccess(drivers));

  } catch (error) {
    console.log('Não foi possível atualizar kilometragem limite')
  }
}

export function* setDrivers() {
  try {
    let drivers = JSON.parse(JSON.stringify(customData));
    drivers.forEach(driver => {
      driver.kmLimit = (Math.random() * (1000 - 300) + 300).toFixed(0);
    });
    yield put(Creators.setDriversSuccess(drivers));
  } catch (error) {
    console.log('Não foi possível atualizar motoristas')
  }
}

export const journeySagas = [
  takeLatest(Types.SET_KM_VALUE_REQUEST, setKmValue),
  takeLatest(Types.SET_KM_LIMIT_REQUEST, setKmLimit),
  takeLatest(Types.UPDATE_DRIVER_JOURNEY_REQUEST, updateDriverJourney),
  takeLatest(Types.SET_DRIVERS_REQUEST, setDrivers),
];
