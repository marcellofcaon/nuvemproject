import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Creators } from '../reducers/journey';

export const useJourneySetKmValueRequest = () => {
  const dispatch = useDispatch();
  return useCallback((kmValue) => {
    dispatch(Creators.setKmValueRequest(kmValue));
  }, [dispatch]);
};

export const useJourneySetKmLimitRequest = () => {
  const dispatch = useDispatch();
  return useCallback((kmLimit) => {
    dispatch(Creators.setKmLimitRequest(kmLimit));
  }, [dispatch]);
};

export const useJourneySetDriversRequest = () => {
  const dispatch = useDispatch();
  return useCallback(() => {
    dispatch(Creators.setDriversRequest());
  }, [dispatch]);
};

export const useJourneySetSelectedDriverRequest = () => {
  const dispatch = useDispatch();
  return useCallback((selectedDriver) => {
    dispatch(Creators.setSelectedDriverRequest(selectedDriver));
  }, [dispatch]);
};

export const useJourneyUpdateDriverJourneyRequest = () => {
  const dispatch = useDispatch();
  return useCallback((distance) => {
    dispatch(Creators.updateDriverJourneyRequest(distance));
  }, [dispatch]);
};

export const useJourneySetHidden = () => {
  const dispatch = useDispatch();
  return useCallback((journeyHidden) => {
    dispatch(Creators.setJourneyHidden(journeyHidden));
  }, [dispatch]);
};

export const useJourneyKmLimit= () => {
  return useSelector((state: any) => state.journey.kmLimit);
};

export const useJourneyKmValue = () => {
  return useSelector((state: any) => state.journey.kmValue);
};

export const useJourneyDrivers = () => {
  return useSelector((state: any) => state.journey.drivers);
};

export const useJourneySelectedDriver = () => {
  return useSelector((state: any) => state.journey.selectedDriver);
};

export const useJourneyHidden = () => {
  return useSelector((state: any) => state.journey.journeyHidden);
};
