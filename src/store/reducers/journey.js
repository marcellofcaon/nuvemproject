import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  getKmValueRequest: null,
  setKmValueRequest: ['kmValue'],
  getKmLimitRequest: null,
  setKmLimitRequest: ['kmLimit'],
  setDriversRequest: null,
  setDriversSuccess: ['drivers'],
  updateDriverJourneyRequest: ['distance'],
  setSelectedDriverRequest: ['selectedDriver'],
  setJourneyHidden: ['journeyHidden']
});

const INITIAL_STATE = {
  kmValue: null,
  kmLimit: false,
  drivers: [],
  selectedDriver: null,
  journeyHidden: true
};

const getKmValueRequest = (state = INITIAL_STATE) => {
  return {
    ...state
  };
};

const setKmValueRequest = (state = INITIAL_STATE, { kmValue }) => ({
  ...state,
  kmValue
});

const setJourneyHidden = (state = INITIAL_STATE, { journeyHidden }) => ({
  ...state,
  journeyHidden
});

const getKmLimitRequest = (state = INITIAL_STATE) => {
  return {
    ...state
  };
};

const setKmLimitRequest = (state = INITIAL_STATE, { kmLimit }) => ({
  ...state,
  kmLimit
});

const setDriversRequest = (state = INITIAL_STATE ) => ({
  ...state
});

const setDriversSuccess = (state = INITIAL_STATE, { drivers }) => ({
  ...state,
  drivers
});

const updateDriverJourneyRequest = (state = INITIAL_STATE, { distance } ) => ({
  ...state
});

const setSelectedDriverRequest = (state = INITIAL_STATE, { selectedDriver }) => ({
  ...state,
  selectedDriver
});

export default createReducer(INITIAL_STATE, {
  [Types.GET_KM_VALUE_REQUEST]: getKmValueRequest,
  [Types.SET_KM_VALUE_REQUEST]: setKmValueRequest,
  [Types.GET_KM_LIMIT_REQUEST]: getKmLimitRequest,
  [Types.SET_KM_LIMIT_REQUEST]: setKmLimitRequest,
  [Types.SET_DRIVERS_REQUEST]: setDriversRequest,
  [Types.SET_DRIVERS_SUCCESS]: setDriversSuccess,
  [Types.UPDATE_DRIVER_JOURNEY_REQUEST]: updateDriverJourneyRequest,
  [Types.SET_SELECTED_DRIVER_REQUEST]: setSelectedDriverRequest,
});
