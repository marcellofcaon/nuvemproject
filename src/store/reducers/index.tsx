import {combineReducers} from 'redux';
import example from './example';
import journey from './journey';

export default combineReducers({
    example,
    journey
});
