import * as React from 'react';
import {Avatar, Icon} from 'react-native-elements';
import {
    ButtonPrimary, ButtonTextPrimary,
    CenteredContainer,
    DistanceInput, Footer,
    HeaderContainer,
    InfoLabel,
    NameLabel,
    ValueByDistanceLabel,
    ValueLabel
} from './styles';
import {
    useJourneyKmValue,
    useJourneySelectedDriver, useJourneySetHidden,
    useJourneyUpdateDriverJourneyRequest
} from '../../store/hooks/journey';
import {Formik} from 'formik';
import {TouchableOpacity} from 'react-native';

const TripDetails = () => {
    const kmValue = useJourneyKmValue();
    const selectedDriver = useJourneySelectedDriver();

    const updateDriver = useJourneyUpdateDriverJourneyRequest();

    const onSubmit = (values: any) => {
        updateDriver(values.distance);
    };

    return (
        <Formik initialValues={{distance: ''}} onSubmit={onSubmit}>
            {({handleChange, handleBlur, handleSubmit, values}) => (
                <>
                    <>
                        <HeaderContainer>
                            <Avatar
                                size={110}
                                rounded
                                source={{uri: selectedDriver ? selectedDriver.img : 'shorturl.at/lpwM7'}}

                            />
                            <CenteredContainer>
                                <ValueByDistanceLabel>R$ {kmValue}/km</ValueByDistanceLabel>
                            </CenteredContainer>
                            <CenteredContainer>
                                <Icon
                                    name="directions-car"
                                    type="material"
                                    color="#FF0000FF"
                                    size={35}
                                />
                            </CenteredContainer>

                        </HeaderContainer>

                        <NameLabel>{selectedDriver ? selectedDriver.nome : ''}</NameLabel>

                        <InfoLabel>Distância da Viagem em km</InfoLabel>
                        <DistanceInput placeholder={'Distância'} autoCapitalize="none"
                                       onChangeText={handleChange('distance')} textContentType={'telephoneNumber'}
                                       onBlur={handleBlur('distance')} value={values.distance}/>

                        <InfoLabel>Km ainda disponível</InfoLabel>
                        <ValueLabel>{selectedDriver?.kmLimit - +values.distance}</ValueLabel>

                        <InfoLabel>Total</InfoLabel>
                        <ValueLabel>R$ {(+values.distance * kmValue).toFixed(2)}</ValueLabel>
                    </>
                    <Footer>
                        <TouchableOpacity style={{width: '100%'}} onPress={() => handleSubmit()}>
                            <ButtonPrimary>
                                <ButtonTextPrimary>Fazer Viagem</ButtonTextPrimary>
                            </ButtonPrimary>
                        </TouchableOpacity>
                    </Footer>
                </>
            )}
        </Formik>
    );
}

export {TripDetails};
