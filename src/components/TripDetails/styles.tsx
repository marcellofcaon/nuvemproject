import styled from 'styled-components/native'

export const HeaderContainer = styled.View`
    justify-content: space-between;
    background: transparent;
    flex-direction: row;
`;

export const NameLabel = styled.Text`
    margin-top: 10px;
    font-size: 18px;
`;

export const CenteredContainer = styled.View`
    justify-content: center;
`;

export const ValueByDistanceLabel = styled.Text`
    font-size: 15px;
    justify-content: center;
`;

export const InfoLabel = styled.Text`
    margin-top: 20px;
    font-size: 12px;
`;

export const ValueLabel = styled.Text`
    font-size: 18px;
`;

export const DistanceInput = styled.TextInput`
    border-radius: 5px;
    border-color: #D8D8D8;
    border-width: 1px;
    background-color: #F7F7F7;
    margin-top: 5px;
    padding: 10px ;
    width: 100px;
`;

export const Footer = styled.View`
    background: transparent;
    position: absolute;
    align-self: center;
    bottom: 10px;
    width: 100%;
    align-items: center;
`;

export const ButtonPrimary = styled.View`
    margin-top: 10px;
    padding: 10px;
    align-items: center;
    border-radius: 10px;
    background: blue;
`;

export const ButtonTextPrimary = styled.Text`
    font-family: Roboto;
    color: white;
    font-size: 15px;
`;
